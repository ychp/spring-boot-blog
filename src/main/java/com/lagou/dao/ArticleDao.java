package com.lagou.dao;

import com.lagou.pojo.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Component;

/**
 * @author yingchengpeng
 */
@Component
public interface ArticleDao extends JpaRepository<Article,Long>, JpaSpecificationExecutor<Article> {

}
