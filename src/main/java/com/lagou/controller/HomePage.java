package com.lagou.controller;

import com.lagou.pojo.Article;
import com.lagou.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author yingchengpeng
 */
@Controller
public class HomePage {

    @Autowired
    private ArticleService articleService;

    /**
     * 跳转到主页面
     */
    @GetMapping("/index")
    public String index(@RequestParam(required = false, defaultValue = "1") Integer pageNo,
                        @RequestParam(required = false, defaultValue = "1") Integer pageSize, Model model) {
        Page<Article> page = articleService.paging(pageNo, pageSize);
        model.addAttribute("page", page);
        model.addAttribute("pageNo", pageNo);
        model.addAttribute("pageSize", pageSize);
        return "client/index";
    }

}
