package com.lagou.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 具体内容存储在详情表
 * @author yingchengpeng
 * @date 2018/08/10
 */
@Data
@Entity
@Table(name = "sky_article")
@ApiModel(description = "文章")
public class Article implements Serializable {

    private static final long serialVersionUID = -4642009075814450402L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "主键", example = "1")
    private Long id;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty(value = "类目ID", example = "1")
    private Long categoryId;

    @ApiModelProperty("类目名称")
    private String categoryName;

    @ApiModelProperty("预览图")
    private String image;

    @ApiModelProperty("简介")
    private String synopsis;

    @ApiModelProperty(value = "作者Id", example = "1")
    private Long userId;

    @ApiModelProperty("作者")
    private String author;

    @ApiModelProperty("状态：0.私有，1.公开，-1.撤下，-99.删除")
    private Integer status;

    @ApiModelProperty("发布时间")
    private Date publishAt;

    private Date createdAt;

    private Date updatedAt;

}