package com.lagou.service;

import com.lagou.pojo.Article;
import org.springframework.data.domain.Page;

/**
 * @author yingchengpeng
 */
public interface ArticleService {

    Page<Article> paging(Integer page, Integer pageSize);
}
