package com.lagou.service.impl;

import com.lagou.dao.ArticleDao;
import com.lagou.pojo.Article;
import com.lagou.service.ArticleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author yingchengpeng
 */
@Service
public class ArticleServiceImpl implements ArticleService {

    @Resource
    private ArticleDao articleDao;

    @Override
    public Page<Article> paging(Integer page, Integer pageSize) {
        Sort sort = Sort.by(Sort.Direction.DESC, "createdAt");
        Pageable pageable = PageRequest.of(page - 1, pageSize, sort);
        Page<Article> page1 = articleDao.findAll(pageable);
        return page1;
    }
}
